﻿#include <iostream>
#include <fstream>

struct T_List
{
    T_List* next;

    int data;
};




void ADD(T_List* head, int data)
{
    T_List* p = new T_List;
    p->data = data;

    p->next = head->next;
    head->next = p;
}


void PRINT(T_List* head)
{
    T_List* p = head->next;
    while (p != nullptr)
    {
        std::cout << p->data << std::endl;
        p = p->next;
    }
}




 void DUPLICATE(int x, T_List* v)
{
     T_List* p = v->next;
     if (p != nullptr)
     {
         if (x == p->data)
         {
             T_List* q = new T_List;
             q->next = p->next;
             p->next = q;
             q->data = p->data;
             p = p->next;

         }
     }
     else return;
}


bool SEARCH(int data, T_List* head)
{
    T_List* v = head->next;
    while (v != nullptr)
    {
        if (v->data == data)
            return true;
        else
            return false;
    }
}


void DELETE(T_List* head, int x)
{
    T_List* tmp;
    T_List* p = head;
    while (p->next != nullptr)
    {
        if (p->next->data == x)
        {
            tmp = p->next;
            p->next = p->next->next;
            delete tmp;
        }
        else
            p = p->next;
    }
}



void CLEAR(T_List* head)
{
    T_List* tmp;
    T_List* p = head->next;
    while (p != nullptr)
    {
        tmp = p;
        p = p->next;
        delete tmp;
    }
}


int main()
{
    int x, d, del; 
    T_List* head = new T_List;
    head->next = nullptr;

  
    std::ifstream in("input.txt");

    std::cin >> d>>del;

 

    while (!in.eof())
    
    {
        in >> x;

        ADD(head, x);

        
            if (SEARCH(d, head))
                DUPLICATE(d, head);

            if (SEARCH(del, head))
                DELETE(head, del);
        
    }
    std::cout << "======"<<std::endl;

    PRINT(head);

    CLEAR(head);

    delete head;

    return 0;
}